module gitlab.com/rojust/dontaskme

require (
	github.com/gin-gonic/gin v1.1.4
	github.com/go-ini/ini v1.37.0
	github.com/golang/protobuf v1.1.0
	github.com/mattn/go-isatty v0.0.3
	gopkg.in/yaml.v2 v2.2.1
)
